package wahidah.hildha.app09_hildha

import android.widget.MediaController
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    val daftarlagu = intArrayOf(R.raw.musik1, R.raw.musik2, R.raw.musik3)
    val daftarjudul = arrayOf("Bright as The Sun", "Everything I Need", "A Whole New World")
    val daftarvideo = intArrayOf(R.raw.video1, R.raw.video2, R.raw.video3)
    val daftarcover = intArrayOf(R.drawable.cover1, R.drawable.cover2, R.drawable.cover3)


    var posLaguSkrg = 0
    var posVideoSkrg = 0
    var durasiLagu = 0
    var handler = Handler()

    lateinit var  mediaPlayer: MediaPlayer
    lateinit var  mediaController: MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max=100
        seekSong.progress= 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        mediaController.setPrevNextListeners(nextVideo,prevVideo)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoSet(posVideoSkrg)
    }

    fun videoSet(pos : Int) {
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarvideo[pos]))
    }

    var nextVideo = View.OnClickListener { v:View ->
    if (posVideoSkrg<(daftarvideo.size-1)) posVideoSkrg++
        else posVideoSkrg= 0
        videoSet(posVideoSkrg)
    }



    var prevVideo = View.OnClickListener { v:View ->
        if (posVideoSkrg>0) posVideoSkrg--
        else posVideoSkrg= daftarvideo.size-1
        videoSet(posVideoSkrg)

    }


    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun  audioPlay (pos : Int){
        mediaPlayer = MediaPlayer.create(this, daftarlagu[pos])
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(daftarcover[pos])
        txJudulLagu.setText(daftarjudul[pos])
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun audioNext() {
        if (mediaPlayer.isPlaying)mediaPlayer.stop()
        if (posLaguSkrg<(daftarlagu.size-1)) {
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        audioPlay(posLaguSkrg)
    }

    fun audioPrev() {
        if (mediaPlayer.isPlaying)mediaPlayer.stop()
        if (posLaguSkrg>0) {
            posLaguSkrg--
        }else {
            posLaguSkrg = daftarlagu.size-1
        }
        audioPlay(posLaguSkrg)
    }

    fun audioStop() {
        if (mediaPlayer.isPlaying)mediaPlayer.stop()

    }

    inner class UpdateSeekBarProgressThread : Runnable {
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress= currTime
            if (currTime != mediaPlayer.duration) handler.postDelayed(this, 50)
        }
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay -> {
                audioPlay(posLaguSkrg)

            }
            R.id.btnNext -> {
                audioNext()

            }
            R.id.btnPrev -> {
                audioPrev()

            }
            R.id.btnStop -> {
                audioStop()

            }
        }
        }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it)}
        }
}
